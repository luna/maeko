defmodule Maeko.MixProject do
  use Mix.Project

  def project do
    [
      app: :maeko,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Maeko, []},
      applications: [:cowboy, :ranch],
      extra_applications: [:logger, :mime]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "~> 2.8.0"},
      {:jason, "~> 1.2"},
      {:mime, "~> 1.4"}
    ]
  end
end
