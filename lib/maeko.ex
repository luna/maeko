defmodule Maeko.DefaultHandler do
  require Logger

  def init(req0, state) do
    req =
      :cowboy_req.reply(
        200,
        %{"content-type" => "text/plain"},
        "awooooooooo",
        req0
      )

    {:ok, req, state}
  end
end

defmodule Maeko.UploadHandler do
  require Logger

  alias Maeko.Utils

  def init(req0, state) do
    token = :cowboy_req.header("authorization", req0, nil)
    wanted_token = Application.fetch_env!(:maeko, :token)

    if token != wanted_token do
      req = Utils.jsonify(req0, %{error: true, message: "invalid token"})
      {:ok, req, state}
    else
      handle_upload(req0, state)
    end
  end

  defp generate_file_name(length) do
    0..length |> Enum.map(fn _ -> Enum.random(97..122) end) |> List.to_string()
  end

  # Fetch the part that contains the file from this request.
  defp fetch_file_part(req0) do
    case :cowboy_req.read_part(req0) do
      {:ok, headers, req1} ->
        case :cow_multipart.form_data(headers) do
          {:data, _field_name} ->
            {:ok, _body, req2} = :cowboy_req.read_part_body(req1)
            # fetched some random data, try to read another part and hope
            # we get the file part as well
            fetch_file_part(req2)

          {:file, field_name, file_name, content_type} ->
            {:ok, {field_name, file_name, content_type}, req1}
        end

      {:done, req} ->
        {:none, nil, req}
    end
  end

  defp read_part_to_file(req0, file) do
    case :cowboy_req.read_part_body(req0) do
      {:ok, data, req} ->
        Logger.info("ok #{String.length(data)}")
        :ok = IO.binwrite(file, data)
        req

      {:more, data, req} ->
        Logger.info("more #{String.length(data)}")
        :ok = IO.binwrite(file, data)
        read_part_to_file(req, file)
    end
  end

  defp handle_upload_with_part(req0, file_name, file_part, state) do
    {_field_name, _file_name, content_type} = file_part
    possible_extensions = MIME.extensions(content_type)

    # assumes the content type is valid
    [ext | _] = possible_extensions

    file = File.open!("images/#{file_name}.#{ext}", [:write, :binary])

    req1 =
      req0
      |> read_part_to_file(file)
      |> Utils.jsonify(%{url: "http://localhost:8081/i/#{file_name}.#{ext}"})

    {:ok, req1, state}
  end

  def handle_upload(req0, state) do
    file_name = generate_file_name(10)
    Logger.info("got upload! #{file_name}")

    case fetch_file_part(req0) do
      {:ok, file_part, req1} ->
        handle_upload_with_part(req1, file_name, file_part, state)

      {:none, req1} ->
        req2 = Utils.jsonify(req1, %{error: true, message: "missing file part"}, 400)
        {:ok, req2, state}
    end
  end
end

defmodule Maeko.FileHandler do
  require Logger

  alias Maeko.Utils

  def init(req0, state) do
    file_name = :cowboy_req.binding(:file, req0)
    IO.puts("got req for #{file_name}")

    path = "images/" <> file_name

    case File.open(path, [:read, :binary]) do
      {:ok, file} ->
        # determine content type based on file extension
        content_type =
          file_name
          |> String.split(".")
          |> tl
          |> hd
          |> MIME.type()

        stat = File.stat!(path)

        req1 =
          :cowboy_req.stream_reply(
            200,
            %{
              "content-type" => content_type,
              "content-length" => "#{stat.size}"
            },
            req0
          )

        write_file_to_body(req1, file)
        {:ok, req1, state}

      {:error, :enoent} ->
        req1 = Utils.jsonify(req0, %{error: true, message: "file not found"}, 404)
        {:ok, req1, state}
    end
  end

  defp write_file_to_body(req1, file) do
    case IO.binread(file, 1024) do
      :eof ->
        :cowboy_req.stream_body("", :nofin, req1)

      {:error, reason} ->
        Logger.error("error while streaming file: #{reason}")
        :cowboy_req.stream_body("", :nofin, req1)

      data ->
        :cowboy_req.stream_body(data, :nofin, req1)
        write_file_to_body(req1, file)
    end
  end
end

defmodule Maeko do
  use Application

  def start(_mode, _args) do
    File.mkdir_p!("images")

    dispatch_config =
      :cowboy_router.compile([
        {:_,
         [
           {"/", Maeko.DefaultHandler, []},
           {"/upload", Maeko.UploadHandler, []},
           {"/i/:file", Maeko.FileHandler, []}
         ]}
      ])

    IO.inspect(dispatch_config)

    :cowboy.start_clear(
      :maeko_http,
      [
        port: 8081
      ],
      %{env: %{dispatch: dispatch_config}}
    )
  end
end
