defmodule Maeko.Utils do
  def jsonify(req, data, status_code) do
    encoded = Jason.encode!(data)

    :cowboy_req.reply(
      status_code,
      %{"content-type" => "application/json"},
      encoded,
      req
    )
  end

  def jsonify(req, data) do
    jsonify(req, data, 200)
  end
end
